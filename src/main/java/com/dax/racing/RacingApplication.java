package com.dax.racing;

import com.dax.racing.constants.Constants;
import com.dax.racing.service.RacingService;
import com.dax.racing.utils.SpringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RacingApplication {
    private static Logger logger = LoggerFactory.getLogger(RacingApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RacingApplication.class, args);

        //启动即加载最近 N 天的数据
//        racingDataCrawling(args);
    }

    /**
     * @Author dinghui
     * @Description 赛车数据爬取
     * @Date 0:36 2023/12/3
     * @Param [args]
     * @return void
     **/
//    private static void racingDataCrawling(String[] args) {
//        logger.info("【赛车】数据爬取：开始……");
//        long currentTime = System.currentTimeMillis();
//
//        //此处main方法中，无法自动注入，需手动注入
//        ApplicationContext context = SpringUtil.getApplicationContext();
//        RacingService racingService = context.getBean(RacingService.class);
//        racingService.getRacingDataWithDayNum(Constants.WEB_LOGIN_COOKIE, Constants.LOAD_DAY_NUM);
//        logger.info("【赛车】数据爬取：共耗时：" + (System.currentTimeMillis() - currentTime) + "ms。");
//
////        racingService.getLastResult(Constants.WEB_LOGIN_COOKIE);
//
////        int num = racingService.addNum(1);
////        logger.info("【赛车】num 1：" + num);
////        num = racingService.addNum(2);
////        logger.info("【赛车】num 2：" + num);
////        num = racingService.addNum(3);
////        logger.info("【赛车】num 3：" + num);
//
////        logger.info("【赛车】数据爬取：关闭此进程。");
////        System.exit(0);
//    }
}
