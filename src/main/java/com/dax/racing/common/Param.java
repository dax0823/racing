package com.dax.racing.common;

import com.dax.racing.constants.Constants;
import lombok.Data;

/**
 * @ClassName Param
 * @Deacription 页面请求参数
 * @Author dinghui
 * @Date 2023/12/6 23:29
 * @Version 1.0
 **/
@Data
public class Param {
    private static final long serialVersionUID = 1L;

    /**
     * 页面上输入的 cookie
     **/
    private String cookie;

    /**
     * 页面上输入的，首次加载取 N 天的数据
     **/
    private int dayNum = Constants.LOAD_DAY_NUM;

    /**
     * 官网地址
     **/
    private String siteAddress = Constants.SITE_ADDRESS_DEFAULT;
}
