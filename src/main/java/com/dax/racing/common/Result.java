package com.dax.racing.common;

import com.dax.racing.constants.Constants;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author dinghui
 * @Description 返回值结构体
 * @Date 12:51 2021/5/17
 * @Param
 * @return
 **/
@Data
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 成功标志
	 */
	private boolean success = true;

	/**
	 * 返回处理消息
	 */
	private String message = "操作成功！";

	/**
	 * 返回代码
	 */
	private Integer code = 0;
	
	/**
	 * 返回数据对象 data
	 */
	private T result;
	
	/**
	 * 时间戳
	 */
	private long timestamp = System.currentTimeMillis();

	public Result() {
	}
	
	public static<T> Result<T> success() {
		Result<T> r = new Result<T>();
		r.setSuccess(true);
		r.setCode(Constants.SC_SUCCESS_200);
		r.setMessage("成功");
		return r;
	}

	public static<T> Result<T> success(T data) {
		Result<T> r = new Result<T>();
		r.setSuccess(true);
		r.setCode(Constants.SC_SUCCESS_200);
		r.setResult(data);
		return r;
	}

	public static<T> Result<T> success(String msg) {
		Result<T> r = new Result<T>();
		r.setSuccess(true);
		r.setCode(Constants.SC_SUCCESS_200);
		r.setMessage(msg);
		return r;
	}

	public static<T> Result<T> success(String msg, T data) {
		Result<T> r = new Result<T>();
		r.setSuccess(true);
		r.setCode(Constants.SC_SUCCESS_200);
		r.setMessage(msg);
		r.setResult(data);
		return r;
	}
	
	public static<T> Result<T> failure(String msg) {
		return failure(Constants.SC_INTERNAL_SERVER_ERROR_500, msg);
	}
	
	public static<T> Result<T> failure(int code, String msg) {
		Result<T> r = new Result<T>();
		r.setCode(code);
		r.setMessage(msg);
		r.setSuccess(false);
		return r;
	}
}