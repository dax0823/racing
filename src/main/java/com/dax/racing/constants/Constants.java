package com.dax.racing.constants;

/**
 * @Author dinghui
 * @Description 常量
 * @Date 12:52 2021/5/17
 * @Param
 * @return
 **/
public interface Constants {
	/** {@code 500 Server Error} (HTTP/1.0 - RFC 1945) */
    Integer SC_INTERNAL_SERVER_ERROR_500 = 500;
    /** {@code 200 OK} (HTTP/1.0 - RFC 1945) */
    Integer SC_SUCCESS_200 = 200;


    /**
     * 账号信息（暂未用上）
     */
    String LOGIN_USERNAME = "susu4566";
//    String LOGIN_USERNAME = "susu4567";
    String LOGIN_PASSWORD = "580623Fei";

    //需加载最近 N 天数据
    int LOAD_DAY_NUM = 3;

    //每次开奖的时间间隔
//    int TIME_INTERVAL = 75;
    int TIME_INTERVAL = 20; //为了避免遗漏和节奏慢于实际开奖频率，将间隔改为 20s 一次

    /**
     * 【赛车】请求地址
     **/
    //赛车请求地址中待替换的字符串
    String REQUEST_URL_RACING_REPLACE_DATE = "#REPLACE_DATE#";

    /*
     * 【www.f661.com】
     **/
    //按日期获取开奖数据的请求 url
//    String REQUEST_URL_RACING_BY_DATE = "https://www.f661.com/member/dresult?lottery=PK10JSC&date=" + REQUEST_URL_RACING_REPLACE_DATE + "&table=1";
//    //最后一次开奖的请求 url
//    String REQUEST_URL_RACING_LAST_RESULT = "https://www.f661.com/member/lastResult?lottery=PK10JSC&_=1701619281572";

    /*
     * 【www.f077.com】
     **/
//    String REQUEST_URL_RACING_BY_DATE = "https://www.f077.com/member/dresult?lottery=PK10JSC&date=" + REQUEST_URL_RACING_REPLACE_DATE + "&table=1";
//    String REQUEST_URL_RACING_LAST_RESULT = "https://www.f077.com/member/lastResult?lottery=PK10JSC&_=1705242840177";

    /*
     * 【www.f718.com】
     **/
//    String REQUEST_URL_RACING_BY_DATE = "https://www.f718.com/member/dresult?lottery=PK10JSC&date=" + REQUEST_URL_RACING_REPLACE_DATE + "&table=1";
//    String REQUEST_URL_RACING_LAST_RESULT = "https://www.f718.com/member/lastResult?lottery=PK10JSC&_=1705242840177";

    /*
     * 默认官网地址
     * 由于官网地址经常更换，所以在页面上增加一个输入框，专门将新的官网地址传入
     * 若为空，则使用下面的默认地址
     **/
//    String SITE_ADDRESS_DEFAULT = "https://www.f661.com";
    String SITE_ADDRESS_DEFAULT = "https://www.f718.com";
    String REQUEST_URL_RACING_BY_DATE = "/member/dresult?lottery=PK10JSC&date=" + REQUEST_URL_RACING_REPLACE_DATE + "&table=1";;
    String REQUEST_URL_RACING_LAST_RESULT = "/member/lastResult?lottery=PK10JSC&_=" + System.currentTimeMillis();


    /**
     * 模拟登录，所需 cookie
     **/
//    String WEB_LOGIN_COOKIE = "08bb96667dc9=16dac15794645074ae5077b96f7dd11e3da6906a; _skin_=red; defaultSetting=5%2C10%2C20%2C50%2C100%2C200%2C500%2C1000; settingChecked=0; defaultLT=PK10JSC; visid_incap_3007845=r7wdhNqfTE+O6yO0AFLynHyxZGUAAAAAQUIPAAAAAAAftjR2MB2lA8dy1phw+Ju+; nlbi_3007845=7w47dCfePiYuTf3Cbt5ztwAAAADsat7uyG2Ng6oE1fD2Xhlk; incap_ses_1205_3007845=BlC0BxaHaiLZMdsENQa5EBznbWUAAAAAsuv10/CXT/01eqS/6p0djg==; ssid1=9a0ade95257fc98ba97e674f62d56140; random=6100; affid=null; token=16dac15794645074ae5077b96f7dd11e3da6906a";

    /**
     * 返回值相关
     **/
    String RETURN_ERROR_MSG = "抱歉，出错啦！";

}
