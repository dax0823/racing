package com.dax.racing.controller;

import com.alibaba.fastjson.JSONObject;
import com.dax.racing.RacingApplication;
import com.dax.racing.common.Param;
import com.dax.racing.common.Result;
import com.dax.racing.service.RacingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName RacingController
 * @Deacription TODO
 * @Author dinghui
 * @Date 2023/12/6 0:31
 * @Version 1.0
 **/
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/racing")
public class RacingController {
    private static Logger logger = LoggerFactory.getLogger(RacingController.class);

    @Autowired
    private RacingService racingService;

    @GetMapping("/test")
    public String test() {
        return "test";
    }

    /**
     * @Author dinghui
     * @Description 页面首次启动
     * @Date 0:12 2023/12/7
     * @Param [param]
     * @return com.dax.racing.common.Result
     **/
    @PostMapping("/begin")
    public Result begin(@RequestBody Param param) {
        logger.info(JSONObject.toJSONString(param));
        return racingService.begin(param);
    }

    /**
     * @Author dinghui
     * @Description 页面定时器，获取最新的各方案计算结果
     * @Date 22:31 2023/12/7
     * @Param []
     * @return com.dax.racing.common.Result
     **/
    @GetMapping("/getNewResult")
    public Result getNewResult() {
        return racingService.getNewResult();
    }

    /**
     * @Author dinghui
     * @Description 获取后端配置文件中的默认官网地址
     * @Date 22:02 2024/2/25
     * @Param []
     * @return com.dax.racing.common.Result
     **/
    @GetMapping("/getSiteAddressDefault")
    public Result getSiteAddressDefault() {
        return racingService.getSiteAddressDefault();
    }
    
}
