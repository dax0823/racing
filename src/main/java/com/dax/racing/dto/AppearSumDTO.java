package com.dax.racing.dto;

import lombok.Data;

/**
 * @Author dinghui
 * @Description 冷/热车号出现次数累加和的对象
 * @Date 23:43 2024/1/4
 * @Param
 * @return
 **/
@Data
public class AppearSumDTO {
    //冷号出现次数累加和
    private int coldNumAppearSum;
    //热号出现次数累加和
    private int hotNumAppearSum;

    public AppearSumDTO(int coldNumAppearSum, int hotNumAppearSum) {
        this.coldNumAppearSum = coldNumAppearSum;
        this.hotNumAppearSum = hotNumAppearSum;
    }
}
