package com.dax.racing.dto;

import lombok.Data;

/**
 * @ClassName CarDTO
 * @Deacription 车辆对象
 * @Author dinghui
 * @Date 2023/12/7 22:51
 * @Version 1.0
 **/
@Data
public class CarDTO {
    //车辆号码
    private int carNum;
    //出现次数
    private int appearCount;

    public CarDTO(int carNum, int appearCount) {
        this.carNum = carNum;
        this.appearCount = appearCount;
    }
}
