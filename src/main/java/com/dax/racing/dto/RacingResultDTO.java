package com.dax.racing.dto;

import lombok.Data;

/**
 * @ClassName RacingDTO
 * @Deacription 每期开奖结果
 * @Author dinghui
 * @Date 2023/12/3 1:37
 * @Version 1.0
 **/
@Data
public class RacingResultDTO {
    //期数
    private long period;

    //开奖时间（格式：yyyy-MM-dd HH:mm:ss）
    private String drawTime;

    /*
     * 各车道车辆编号
     **/
    //车道 1 的车辆编号
    private int lane1CarNum;
    //车道 2 的车辆编号
    private int lane2CarNum;
    //车道 3 的车辆编号
    private int lane3CarNum;
    //车道 4 的车辆编号
    private int lane4CarNum;
    //车道 5 的车辆编号
    private int lane5CarNum;
    //车道 6 的车辆编号
    private int lane6CarNum;
    //车道 7 的车辆编号
    private int lane7CarNum;
    //车道 8 的车辆编号
    private int lane8CarNum;
    //车道 9 的车辆编号
    private int lane9CarNum;
    //车道 10 的车辆编号
    private int lane10CarNum;

    /*
     * 冠亚军
     **/
    //冠亚军之和
    private int gySum;
    //冠亚军之和的大小：大（> 10） or 小（<= 10）
    private String gyIsBig;
    //冠亚军之和的单双：单 or 双
    private String gyIsSingular;

    /*
     * 龙虎榜
     **/
    //车道 1 的龙虎：龙（车道 1 > 车道 10）or 虎（车道 1 < 车道 10）
    private String lane1LH;
    //车道 2 的龙虎：龙（车道 2 > 车道 9）or 虎（车道 2 < 车道 9）
    private String lane2LH;
    //车道 3 的龙虎：龙（车道 3 > 车道 8）or 虎（车道 3 < 车道 8）
    private String lane3LH;
    //车道 4 的龙虎：龙（车道 4 > 车道 7）or 虎（车道 4 < 车道 7）
    private String lane4LH;
    //车道 5 的龙虎：龙（车道 5 > 车道 6）or 虎（车道 5 < 车道 6）
    private String lane5LH;
}
