package com.dax.racing.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName Schema1DTO
 * @Deacription 方案一的对象
 * @Author dinghui
 * @Date 2023/12/6 23:43
 * @Version 1.0
 **/
@Data
public class Schema1DTO {
    private List<CarDTO> lane1CarSort;
    private List<CarDTO> lane2CarSort;
    private List<CarDTO> lane3CarSort;
    private List<CarDTO> lane4CarSort;
    private List<CarDTO> lane5CarSort;
    private List<CarDTO> lane6CarSort;
    private List<CarDTO> lane7CarSort;
    private List<CarDTO> lane8CarSort;
    private List<CarDTO> lane9CarSort;
    private List<CarDTO> lane10CarSort;
}
