package com.dax.racing.dto;

import lombok.Data;


/**
 * @ClassName Schema2DTO
 * @Deacription 方案二冷/热号出现次数累加和的对象
 * @Author dinghui
 * @Date 2023/12/6 23:44
 * @Version 1.0
 **/
@Data
public class Schema2AppearColdCountDTO {
    private int car1AppearColdCount;
    private int car2AppearColdCount;
    private int car3AppearColdCount;
    private int car4AppearColdCount;
    private int car5AppearColdCount;
    private int car6AppearColdCount;
    private int car7AppearColdCount;
    private int car8AppearColdCount;
    private int car9AppearColdCount;
    private int car10AppearColdCount;
}
