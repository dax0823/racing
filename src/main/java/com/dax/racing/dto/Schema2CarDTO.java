package com.dax.racing.dto;

import lombok.Data;

/**
 * @ClassName Schema2CarDTO
 * @Deacription 方案二的车辆对象
 * @Author dinghui
 * @Date 2023/12/9 0:31
 * @Version 1.0
 **/
@Data
public class Schema2CarDTO {
    //车辆号
    private int carNum;
    //当前车道号
    private int currentLaneNum;
    //车辆 1 对象
    private CarDTO car1;
    //车辆 2 对象
    private CarDTO car2;
    //车辆 3 对象
    private CarDTO car3;
    //车辆 4 对象
    private CarDTO car4;
    //车辆 5 对象
    private CarDTO car5;
    //车辆 6 对象
    private CarDTO car6;
    //车辆 7 对象
    private CarDTO car7;
    //车辆 8 对象
    private CarDTO car8;
    //车辆 9 对象
    private CarDTO car9;
    //车辆 10 对象
    private CarDTO car10;
}
