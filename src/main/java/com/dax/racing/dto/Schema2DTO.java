package com.dax.racing.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName Schema2DTO
 * @Deacription 方案二的对象
 * @Author dinghui
 * @Date 2023/12/6 23:44
 * @Version 1.0
 **/
@Data
public class Schema2DTO {
    private List<CarDTO> car1CarSort;
    private List<CarDTO> car2CarSort;
    private List<CarDTO> car3CarSort;
    private List<CarDTO> car4CarSort;
    private List<CarDTO> car5CarSort;
    private List<CarDTO> car6CarSort;
    private List<CarDTO> car7CarSort;
    private List<CarDTO> car8CarSort;
    private List<CarDTO> car9CarSort;
    private List<CarDTO> car10CarSort;
}
