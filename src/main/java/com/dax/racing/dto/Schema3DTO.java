package com.dax.racing.dto;

import lombok.Data;

/**
 * @ClassName Schema3DTO
 * @Deacription 方案三的对象
 * @Author dinghui
 * @Date 2023/12/6 23:44
 * @Version 1.0
 **/
@Data
public class Schema3DTO {
    private Schema3LaneDTO lane1CountDTO;
    private Schema3LaneDTO lane2CountDTO;
    private Schema3LaneDTO lane3CountDTO;
    private Schema3LaneDTO lane4CountDTO;
    private Schema3LaneDTO lane5CountDTO;
    private Schema3LaneDTO lane6CountDTO;
    private Schema3LaneDTO lane7CountDTO;
    private Schema3LaneDTO lane8CountDTO;
    private Schema3LaneDTO lane9CountDTO;
    private Schema3LaneDTO lane10CountDTO;
}
