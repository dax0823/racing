package com.dax.racing.dto;

import lombok.Data;

/**
 * @ClassName LaneDTO
 * @Deacription 方案一的车道对象
 * @Author dinghui
 * @Date 2023/12/7 22:41
 * @Version 1.0
 **/
@Data
public class Schema3LaneDTO {
    //正确结果次数对象
    private Schema3ResultCountDTO correctCountDTO;
    //错误结果次数对象
    private Schema3ResultCountDTO errorCountDTO;
}
