package com.dax.racing.dto;

import lombok.Data;

/**
 * @ClassName Schema3CorrectDTO
 * @Deacription 方案三的车道的（正确/错误）累计结果对象
 * @Author dinghui
 * @Date 2023/12/9 19:32
 * @Version 1.0
 **/
@Data
public class Schema3ResultCountDTO {
    //出现【大】数的次数
    private int bigCount;
    //出现【小】数的次数
    private int smallCount;
    //出现【单】数（奇数）的次数
    private int oddCount;
    //出现【双】数（偶数）的次数
    private int evenCount;

    public Schema3ResultCountDTO(int bigCount, int smallCount, int oddCount, int evenCount) {
        this.bigCount = bigCount;
        this.smallCount = smallCount;
        this.oddCount = oddCount;
        this.evenCount = evenCount;
    }
}