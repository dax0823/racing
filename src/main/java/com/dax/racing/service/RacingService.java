package com.dax.racing.service;

import com.dax.racing.common.Param;
import com.dax.racing.common.Result;
import com.dax.racing.vo.ResponseVO;

public interface RacingService {

    /**
     * @Author dinghui
     * @Description 获取最后一次开奖结果，并将其填入全量数据（内存）中
     * @Date 0:20 2023/12/4
     * @Param []
     * @return void
     **/
    void getLastResult();

    /**
     * @Author dinghui
     * @Description 首次启动
     * @Date 23:48 2023/12/6
     * @Param [param]
     * @return com.dax.racing.common.Result<com.dax.racing.vo.PageResultVO>
     **/
    Result<ResponseVO> begin(Param param);

    /**
     * @Author dinghui
     * @Description 取最新的各方案计算结果
     * @Date 22:28 2023/12/7
     * @Param []
     * @return com.dax.racing.common.Result<com.dax.racing.vo.ResponseVO>
     **/
    Result<ResponseVO> getNewResult();

    /**
     * @Author dinghui
     * @Description 获取后端配置文件中的默认官网地址
     * @Date 22:28 2023/12/7
     * @Param []
     * @return com.dax.racing.common.Result<com.dax.racing.vo.ResponseVO>
     **/
    Result<String> getSiteAddressDefault();

}
