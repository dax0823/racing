package com.dax.racing.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dax.racing.common.Param;
import com.dax.racing.common.Result;
import com.dax.racing.constants.Constants;
import com.dax.racing.dto.*;
import com.dax.racing.service.RacingService;
import com.dax.racing.utils.DateUtils;
import com.dax.racing.utils.HtmlUtils;
import com.dax.racing.vo.ResponseVO;
import com.gargoylesoftware.htmlunit.WebClient;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName RacingServiceImpl
 * @Deacription 获取【赛车数据】实现类
 * @Author dinghui
 * @Date 2023/12/3 0:40
 * @Version 1.0
 **/
@Service
public class RacingServiceImpl implements RacingService {
    private static Logger logger = LoggerFactory.getLogger(RacingServiceImpl.class);

    //web 客户端对象
    private WebClient webClient;

    //页面登录后的 cookie
    @Setter
    @Getter
    private String webCookie;

    //首次启动时，需加载的天数
    @Setter
    @Getter
    private int dayNum;

    //内存中存储全部数据
    private List<RacingResultDTO> allList = new ArrayList<>(16);

    //官网地址
    private String siteAddress = "";

    /**
     * @Author dinghui
     * @Description 创建web客户端
     * @Date 0:23 2023/12/4
     * @Param []
     * @return com.gargoylesoftware.htmlunit.WebClient
     **/
    private WebClient getWebClient() {
        if (webClient == null) {
            try {
                webClient = HtmlUtils.createWebClient();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return webClient;
    }

    @Override
    public void getLastResult() {
        if (StringUtils.isBlank(getWebCookie())) {
            logger.info("当前 webCookie 为空，[getLastResult()]不请求数据。");
            //当 webCookie 为空时，仅空跑，不执行业务处理（服务启动了，但用户可能还未准备完毕）
            //nothing todo
        } else {
            //获取数据
            try {
//                String lastResultJson = HtmlUtils.sendRequestRacingLastResult(Constants.REQUEST_URL_RACING_LAST_RESULT, getWebClient(), getWebCookie());
                String lastResultJson = HtmlUtils.sendRequestRacingLastResult(siteAddress.substring(0, siteAddress.indexOf(".com") + 4) + Constants.REQUEST_URL_RACING_LAST_RESULT, getWebClient(), getWebCookie());
                logger.info("lastResultJson：" + lastResultJson);
                RacingResultDTO lastResultDTO = HtmlUtils.parsingLastResultJson(lastResultJson);
                logger.info("getLastResult：" + JSONObject.toJSONString(lastResultDTO));

                //若全量数据中不含此最后一次开奖内容，则将其添加到全量数据中，以备后用
                if (lastResultDTO != null) {
                    List<Long> periodList = allList.stream().map(x -> x.getPeriod()).collect(Collectors.toList());
                    if (!periodList.contains(lastResultDTO.getPeriod())) {
                        //将最新一期的开奖结果，插到第一位
//                        allList.add(lastResultDTO);
                        allList.add(0, lastResultDTO);
                    }
                }
            } catch (Exception e) {
                logger.error("处理异常，将 cookie 置空，不再重复错误请求。请联系管理员。", e);
                setWebCookie(null);
                e.printStackTrace();
            }
        }
    }

    @Override
    public Result<ResponseVO> begin(Param param) {
        //校验参数
        if (param == null) {
            return Result.failure(-1, "页面参数传输错误，请联系管理员。");
        } else {
            if (StringUtils.isBlank(param.getCookie())) {
                return Result.failure(-1, "参数中缺少 cookie。");
            }
            if (param.getDayNum() < 0) {
                //当传入的天数小于 0 时（可以为 0 天），设置为默认值
                param.setDayNum(Constants.LOAD_DAY_NUM);
            }
            if (StringUtils.isNotBlank(param.getSiteAddress())) {
                siteAddress = param.getSiteAddress();
            }
        }

        //清理内存中已有的开奖数据
        allList = new ArrayList<>(16);

        /*
         * 1. 将页面上传来的 cookie 保存于内存中
         **/
        setWebCookie(param.getCookie());

        /*
         * 2. 将页面上传来的 dayNum 保存于内存中
         **/
        setDayNum(param.getDayNum());

        /*
         * 3. 按天数获取开奖结果，并存于内存中
         **/
        getRacingDataWithDayNum(getWebCookie(), getDayNum());

        /*
         * 4. 计算页面上需要返回的各方案内容
         **/
        //先将数据做一个反向排序，最晚一期在最上面
        allList = allList.stream().sorted(Comparator.comparingLong(RacingResultDTO::getPeriod).reversed()).collect(Collectors.toList());
//        logger.info("全量数据：" + JSONObject.toJSONString(allList));

        return Result.success(getResponse());
    }

    @Override
    public Result<ResponseVO> getNewResult() {
        return Result.success(getResponse());
    }

    @Override
    public Result<String> getSiteAddressDefault() {
        //先将默认值填入缓存，待后续真正启动时再修改
        siteAddress = Constants.SITE_ADDRESS_DEFAULT;
        return Result.success("默认官网地址", siteAddress);
    }

    /**
     * @Author dinghui
     * @Description 获取全量数据的计算结果
     * @Date 0:27 2023/12/9
     * @Param []
     * @return com.dax.racing.vo.ResponseVO
     **/
    private ResponseVO getResponse() {
        ResponseVO responseVO = new ResponseVO();
        if (!CollectionUtils.isEmpty(allList)) {
            responseVO.setLoadDayNum(dayNum);
            responseVO.setPeriodCount(allList.size());
            responseVO.setLastRacingResultDTO(allList.get(0));
//            responseVO.setSchema1DTO(calculateSchema1Result());
            responseVO.setSchema2DTO(calculateSchema2Result());
            responseVO.setSchema2AppearColdCountDTO(calculateSchema2AppearColdCountResult(responseVO.getSchema2DTO()));
//            responseVO.setSchema3DTO(calculateSchema3Result());
        }
        return responseVO;
    }

    /**
     * @Author dinghui
     * @Description 计算方案一的结果
     * 【方案一】
     *  每条车道的热号（出现频率最高的前五个号码）
     *
     * @Date 0:28 2023/12/7
     * @Param []
     * @return com.dax.racing.dto.Schema1DTO
     **/
    private Schema1DTO calculateSchema1Result() {
        Schema1DTO schema1DTO = null;
        if (!CollectionUtils.isEmpty(allList)) {
            //车道 1
            Schema1LaneDTO lane1 = createSchema1CarDto(null);
            //车道 2
            Schema1LaneDTO lane2 = createSchema1CarDto(null);
            //车道 3
            Schema1LaneDTO lane3 = createSchema1CarDto(null);
            //车道 4
            Schema1LaneDTO lane4 = createSchema1CarDto(null);
            //车道 5
            Schema1LaneDTO lane5 = createSchema1CarDto(null);
            //车道 6
            Schema1LaneDTO lane6 = createSchema1CarDto(null);
            //车道 7
            Schema1LaneDTO lane7 = createSchema1CarDto(null);
            //车道 8
            Schema1LaneDTO lane8 = createSchema1CarDto(null);
            //车道 9
            Schema1LaneDTO lane9 = createSchema1CarDto(null);
            //车道 10
            Schema1LaneDTO lane10 = createSchema1CarDto(null);

            //逐条车道累计次数
            for (RacingResultDTO racingResultDTO : allList) {
                if (racingResultDTO != null) {
                    sumSchema1AppearCount(lane1, racingResultDTO.getLane1CarNum());
                    sumSchema1AppearCount(lane2, racingResultDTO.getLane2CarNum());
                    sumSchema1AppearCount(lane3, racingResultDTO.getLane3CarNum());
                    sumSchema1AppearCount(lane4, racingResultDTO.getLane4CarNum());
                    sumSchema1AppearCount(lane5, racingResultDTO.getLane5CarNum());
                    sumSchema1AppearCount(lane6, racingResultDTO.getLane6CarNum());
                    sumSchema1AppearCount(lane7, racingResultDTO.getLane7CarNum());
                    sumSchema1AppearCount(lane8, racingResultDTO.getLane8CarNum());
                    sumSchema1AppearCount(lane9, racingResultDTO.getLane9CarNum());
                    sumSchema1AppearCount(lane10, racingResultDTO.getLane10CarNum());
                }
            }

            //整理各条车道，按照各车辆的出现频次降序排序（由大到小）
            schema1DTO = new Schema1DTO();
            schema1DTO.setLane1CarSort(reverseSortSchema1CarsByAppearCount(lane1));
            schema1DTO.setLane2CarSort(reverseSortSchema1CarsByAppearCount(lane2));
            schema1DTO.setLane3CarSort(reverseSortSchema1CarsByAppearCount(lane3));
            schema1DTO.setLane4CarSort(reverseSortSchema1CarsByAppearCount(lane4));
            schema1DTO.setLane5CarSort(reverseSortSchema1CarsByAppearCount(lane5));
            schema1DTO.setLane6CarSort(reverseSortSchema1CarsByAppearCount(lane6));
            schema1DTO.setLane7CarSort(reverseSortSchema1CarsByAppearCount(lane7));
            schema1DTO.setLane8CarSort(reverseSortSchema1CarsByAppearCount(lane8));
            schema1DTO.setLane9CarSort(reverseSortSchema1CarsByAppearCount(lane9));
            schema1DTO.setLane10CarSort(reverseSortSchema1CarsByAppearCount(lane10));

//            logger.info(JSONObject.toJSONString(schema1DTO.getLane10CarSort()));
        }
        return schema1DTO;
    }

    /**
     * @Author dinghui
     * @Description 方案一按照车辆出现次数倒序排序
     * @Date 23:17 2023/12/7
     * @Param [laneDTO]
     * @return java.util.List<com.dax.racing.dto.CarDTO>
     **/
    private List<CarDTO> reverseSortSchema1CarsByAppearCount(Schema1LaneDTO schema1LaneDTO) {
        if (schema1LaneDTO == null) {
            return null;
        }

        //将所有车辆数据填入 list
        List<CarDTO> carList = new ArrayList<CarDTO>(Arrays.asList(schema1LaneDTO.getCar1(), schema1LaneDTO.getCar2(), schema1LaneDTO.getCar3(),
                schema1LaneDTO.getCar4(), schema1LaneDTO.getCar5(), schema1LaneDTO.getCar6(), schema1LaneDTO.getCar7(), schema1LaneDTO.getCar8(),
                schema1LaneDTO.getCar9(), schema1LaneDTO.getCar10()));

        //倒序排列
        carList = carList.stream().sorted(Comparator.comparingInt(CarDTO::getAppearCount).reversed())
                .collect(Collectors.toList());
        return carList;
    }

    /**
     * @Author dinghui
     * @Description 将该条车道出现的车辆号，累加
     * @Date 23:01 2023/12/7
     * @Param [laneDTO, carNum]
     * @return com.dax.racing.dto.LaneDTO
     **/
    private Schema1LaneDTO sumSchema1AppearCount(Schema1LaneDTO schema1LaneDTO, int carNum) {
        if (schema1LaneDTO != null && carNum >= 1 && carNum <= 10) {
            switch (carNum) {
                case 1:
                    schema1LaneDTO.getCar1().setAppearCount(schema1LaneDTO.getCar1().getAppearCount() + 1);
                    break;
                case 2:
                    schema1LaneDTO.getCar2().setAppearCount(schema1LaneDTO.getCar2().getAppearCount() + 1);
                    break;
                case 3:
                    schema1LaneDTO.getCar3().setAppearCount(schema1LaneDTO.getCar3().getAppearCount() + 1);
                    break;
                case 4:
                    schema1LaneDTO.getCar4().setAppearCount(schema1LaneDTO.getCar4().getAppearCount() + 1);
                    break;
                case 5:
                    schema1LaneDTO.getCar5().setAppearCount(schema1LaneDTO.getCar5().getAppearCount() + 1);
                    break;
                case 6:
                    schema1LaneDTO.getCar6().setAppearCount(schema1LaneDTO.getCar6().getAppearCount() + 1);
                    break;
                case 7:
                    schema1LaneDTO.getCar7().setAppearCount(schema1LaneDTO.getCar7().getAppearCount() + 1);
                    break;
                case 8:
                    schema1LaneDTO.getCar8().setAppearCount(schema1LaneDTO.getCar8().getAppearCount() + 1);
                    break;
                case 9:
                    schema1LaneDTO.getCar9().setAppearCount(schema1LaneDTO.getCar9().getAppearCount() + 1);
                    break;
                case 10:
                    schema1LaneDTO.getCar10().setAppearCount(schema1LaneDTO.getCar10().getAppearCount() + 1);
                    break;
                default:
                    logger.error("车辆号码异常：" + carNum);
            }
        }
        return schema1LaneDTO;
    }

    /**
     * @Author dinghui
     * @Description 创建方案一的车辆对象
     * @Date 22:54 2023/12/7
     * @Param [laneDTO]
     * @return com.dax.racing.dto.LaneDTO
     **/
    private Schema1LaneDTO createSchema1CarDto(Schema1LaneDTO schema1LaneDTO) {
        if (schema1LaneDTO == null) {
            schema1LaneDTO = new Schema1LaneDTO();
        }
        schema1LaneDTO.setCar1(new CarDTO(1, 0));
        schema1LaneDTO.setCar2(new CarDTO(2, 0));
        schema1LaneDTO.setCar3(new CarDTO(3, 0));
        schema1LaneDTO.setCar4(new CarDTO(4, 0));
        schema1LaneDTO.setCar5(new CarDTO(5, 0));
        schema1LaneDTO.setCar6(new CarDTO(6, 0));
        schema1LaneDTO.setCar7(new CarDTO(7, 0));
        schema1LaneDTO.setCar8(new CarDTO(8, 0));
        schema1LaneDTO.setCar9(new CarDTO(9, 0));
        schema1LaneDTO.setCar10(new CarDTO(10, 0));

        return schema1LaneDTO;
    }

    /**
     * @Author dinghui
     * @Description 计算方案二的结果
     * 【方案二】
     *  每条车道开出的号码，它的下一期开出的号码频率最高的五个号码
     *  （如：假如 3 道开出来的是 8，每次这个 8 的下一期开出来的最频繁的五个号码）
     *
     * @Date 0:31 2023/12/7
     * @Param []
     * @return com.dax.racing.dto.Schema2DTO
     **/
    private Schema2DTO calculateSchema2Result() {
        Schema2DTO schema2DTO = null;
        if (!CollectionUtils.isEmpty(allList)) {
            //车道 1
            Schema2CarDTO car1 = createSchema2CarDto(null, 1);
            //车道 2,
            Schema2CarDTO car2 = createSchema2CarDto(null, 2);
            //车道 3,
            Schema2CarDTO car3 = createSchema2CarDto(null, 3);
            //车道 4,
            Schema2CarDTO car4 = createSchema2CarDto(null, 4);
            //车道 5,
            Schema2CarDTO car5 = createSchema2CarDto(null, 5);
            //车道 6,
            Schema2CarDTO car6 = createSchema2CarDto(null, 6);
            //车道 7,
            Schema2CarDTO car7 = createSchema2CarDto(null, 7);
            //车道 8,
            Schema2CarDTO car8 = createSchema2CarDto(null, 8);
            //车道 9,
            Schema2CarDTO car9 = createSchema2CarDto(null, 9);
            //车道 10
            Schema2CarDTO car10 = createSchema2CarDto(null, 10);

            //先将第一次开奖结果初始化（填入各个车辆对象中）
            RacingResultDTO racingResultDTO = allList.get(allList.size() - 1);
            initSchema2CarDTO(car1, racingResultDTO);
            initSchema2CarDTO(car2, racingResultDTO);
            initSchema2CarDTO(car3, racingResultDTO);
            initSchema2CarDTO(car4, racingResultDTO);
            initSchema2CarDTO(car5, racingResultDTO);
            initSchema2CarDTO(car6, racingResultDTO);
            initSchema2CarDTO(car7, racingResultDTO);
            initSchema2CarDTO(car8, racingResultDTO);
            initSchema2CarDTO(car9, racingResultDTO);
            initSchema2CarDTO(car10, racingResultDTO);
            //逐条车道累计次数
            for (int i = allList.size()-2; i >= 0; i--) {
                if (allList.get(i) != null) {
                    sumSchema2AppearCount(car1, allList.get(i));
                    sumSchema2AppearCount(car2, allList.get(i));
                    sumSchema2AppearCount(car3, allList.get(i));
                    sumSchema2AppearCount(car4, allList.get(i));
                    sumSchema2AppearCount(car5, allList.get(i));
                    sumSchema2AppearCount(car6, allList.get(i));
                    sumSchema2AppearCount(car7, allList.get(i));
                    sumSchema2AppearCount(car8, allList.get(i));
                    sumSchema2AppearCount(car9, allList.get(i));
                    sumSchema2AppearCount(car10, allList.get(i));
                }
            }

            //整理各条车道，按照各车辆的出现频次降序排序（由大到小）
            schema2DTO = new Schema2DTO();
            schema2DTO.setCar1CarSort(reverseSortSchema2CarsByAppearCount(car1));
            schema2DTO.setCar2CarSort(reverseSortSchema2CarsByAppearCount(car2));
            schema2DTO.setCar3CarSort(reverseSortSchema2CarsByAppearCount(car3));
            schema2DTO.setCar4CarSort(reverseSortSchema2CarsByAppearCount(car4));
            schema2DTO.setCar5CarSort(reverseSortSchema2CarsByAppearCount(car5));
            schema2DTO.setCar6CarSort(reverseSortSchema2CarsByAppearCount(car6));
            schema2DTO.setCar7CarSort(reverseSortSchema2CarsByAppearCount(car7));
            schema2DTO.setCar8CarSort(reverseSortSchema2CarsByAppearCount(car8));
            schema2DTO.setCar9CarSort(reverseSortSchema2CarsByAppearCount(car9));
            schema2DTO.setCar10CarSort(reverseSortSchema2CarsByAppearCount(car10));
//            logger.info(JSONObject.toJSONString(schema2DTO.getCar10CarSort()));
        }

        return schema2DTO;
    }

    /**
     * @Author dinghui
     * @Description 创建方案二的车辆对象
     * @Date 0:36 2023/12/9
     * @Param [schema1LaneDTO]
     * @return com.dax.racing.dto.Schema1LaneDTO
     **/
    private Schema2CarDTO createSchema2CarDto(Schema2CarDTO schema2CarDTO, int carNum) {
        if (schema2CarDTO == null) {
            schema2CarDTO = new Schema2CarDTO();
        }

        //车辆号
        if (carNum < 1 || carNum > 10) {
            return null;
        }
        schema2CarDTO.setCarNum(carNum);

        schema2CarDTO.setCar1(new CarDTO(1, 0));
        schema2CarDTO.setCar2(new CarDTO(2, 0));
        schema2CarDTO.setCar3(new CarDTO(3, 0));
        schema2CarDTO.setCar4(new CarDTO(4, 0));
        schema2CarDTO.setCar5(new CarDTO(5, 0));
        schema2CarDTO.setCar6(new CarDTO(6, 0));
        schema2CarDTO.setCar7(new CarDTO(7, 0));
        schema2CarDTO.setCar8(new CarDTO(8, 0));
        schema2CarDTO.setCar9(new CarDTO(9, 0));
        schema2CarDTO.setCar10(new CarDTO(10, 0));

        return schema2CarDTO;
    }

    /**
     * @Author dinghui
     * @Description 将第一期的基础数据初始化
     * @Date 1:21 2023/12/9
     * @Param [schema2CarDTO, racingResultDTO]
     * @return com.dax.racing.dto.Schema2CarDTO
     **/
    private Schema2CarDTO initSchema2CarDTO(Schema2CarDTO schema2CarDTO, RacingResultDTO racingResultDTO) {
        if (schema2CarDTO != null && racingResultDTO != null) {
            if (schema2CarDTO.getCarNum() == racingResultDTO.getLane1CarNum()) {
                schema2CarDTO.setCurrentLaneNum(1);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane2CarNum()) {
                schema2CarDTO.setCurrentLaneNum(2);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane3CarNum()) {
                schema2CarDTO.setCurrentLaneNum(3);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane4CarNum()) {
                schema2CarDTO.setCurrentLaneNum(4);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane5CarNum()) {
                schema2CarDTO.setCurrentLaneNum(5);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane6CarNum()) {
                schema2CarDTO.setCurrentLaneNum(6);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane7CarNum()) {
                schema2CarDTO.setCurrentLaneNum(7);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane8CarNum()) {
                schema2CarDTO.setCurrentLaneNum(8);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane9CarNum()) {
                schema2CarDTO.setCurrentLaneNum(9);
            } else if (schema2CarDTO.getCarNum() == racingResultDTO.getLane10CarNum()) {
                schema2CarDTO.setCurrentLaneNum(10);
            } else {
                logger.error("车辆号码异常：" + schema2CarDTO.getCarNum());
            }
        }
        return schema2CarDTO;
    }

    /**
     * @Author dinghui
     * @Description 将 N 号车所在车道的下一期车号，出现次数累计
     * @Date 1:19 2023/12/9
     * @Param [schema2CarDTO, racingResultDTO]
     * @return com.dax.racing.dto.Schema2CarDTO
     **/
    private Schema2CarDTO sumSchema2AppearCount(Schema2CarDTO schema2CarDTO, RacingResultDTO racingResultDTO) {
        if (schema2CarDTO != null && racingResultDTO != null) {
            int carNum = 0;
            switch (schema2CarDTO.getCurrentLaneNum()) {
                case 1:
                    carNum = getSchema2CarNum(racingResultDTO.getLane1CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(1);
                    break;
                case 2:
                    carNum = getSchema2CarNum(racingResultDTO.getLane2CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(2);
                    break;
                case 3:
                    carNum = getSchema2CarNum(racingResultDTO.getLane3CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(3);
                    break;
                case 4:
                    carNum = getSchema2CarNum(racingResultDTO.getLane4CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(4);
                    break;
                case 5:
                    carNum = getSchema2CarNum(racingResultDTO.getLane5CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(5);
                    break;
                case 6:
                    carNum = getSchema2CarNum(racingResultDTO.getLane6CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(6);
                    break;
                case 7:
                    carNum = getSchema2CarNum(racingResultDTO.getLane7CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(7);
                    break;
                case 8:
                    carNum = getSchema2CarNum(racingResultDTO.getLane8CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(8);
                    break;
                case 9:
                    carNum = getSchema2CarNum(racingResultDTO.getLane9CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(9);
                    break;
                case 10:
                    carNum = getSchema2CarNum(racingResultDTO.getLane10CarNum(), schema2CarDTO);
                    schema2CarDTO.setCurrentLaneNum(10);
                    break;
                default:
                    logger.error("车辆号码异常：" + carNum);
            }
        }
        return schema2CarDTO;
    }

    /**
     * @Author dinghui
     * @Description 判断车道 N 中的车辆号，并在对应车辆的出现次数上累加
     * @Date 1:39 2023/12/9
     * @Param [lane10CarNum]
     * @return int
     **/
    private int getSchema2CarNum(int laneNCarNum, Schema2CarDTO schema2CarDTO) {
        int carNum = 0;
        switch (laneNCarNum) {
            case 1:
                carNum = 1;
                schema2CarDTO.getCar1().setAppearCount(schema2CarDTO.getCar1().getAppearCount() + 1);
                break;
            case 2:
                carNum = 2;
                schema2CarDTO.getCar2().setAppearCount(schema2CarDTO.getCar2().getAppearCount() + 1);
                break;
            case 3:
                carNum = 3;
                schema2CarDTO.getCar3().setAppearCount(schema2CarDTO.getCar3().getAppearCount() + 1);
                break;
            case 4:
                carNum = 4;
                schema2CarDTO.getCar4().setAppearCount(schema2CarDTO.getCar4().getAppearCount() + 1);
                break;
            case 5:
                carNum = 5;
                schema2CarDTO.getCar5().setAppearCount(schema2CarDTO.getCar5().getAppearCount() + 1);
                break;
            case 6:
                carNum = 6;
                schema2CarDTO.getCar6().setAppearCount(schema2CarDTO.getCar6().getAppearCount() + 1);
                break;
            case 7:
                carNum = 7;
                schema2CarDTO.getCar7().setAppearCount(schema2CarDTO.getCar7().getAppearCount() + 1);
                break;
            case 8:
                carNum = 8;
                schema2CarDTO.getCar8().setAppearCount(schema2CarDTO.getCar8().getAppearCount() + 1);
                break;
            case 9:
                carNum = 9;
                schema2CarDTO.getCar9().setAppearCount(schema2CarDTO.getCar9().getAppearCount() + 1);
                break;
            case 10:
                carNum = 10;
                schema2CarDTO.getCar10().setAppearCount(schema2CarDTO.getCar1().getAppearCount() + 1);
                break;
            default:
                logger.error("车辆号码异常：" + carNum);
        }
        return carNum;
    }

    /**
     * @Author dinghui
     * @Description 方案二按照车辆出现次数倒序排序
     * @Date 1:49 2023/12/9
     * @Param [schema2CarDTO]
     * @return java.util.List<com.dax.racing.dto.CarDTO>
     **/
    private List<CarDTO> reverseSortSchema2CarsByAppearCount(Schema2CarDTO schema2CarDTO) {
        if (schema2CarDTO == null) {
            return null;
        }

        //将所有车辆数据填入 list
        List<CarDTO> carList = new ArrayList<CarDTO>(Arrays.asList(schema2CarDTO.getCar1(), schema2CarDTO.getCar2(), schema2CarDTO.getCar3(),
                schema2CarDTO.getCar4(), schema2CarDTO.getCar5(), schema2CarDTO.getCar6(), schema2CarDTO.getCar7(), schema2CarDTO.getCar8(),
                schema2CarDTO.getCar9(), schema2CarDTO.getCar10()));

        //倒序排列
        carList = carList.stream().sorted(Comparator.comparingInt(CarDTO::getAppearCount).reversed())
                .collect(Collectors.toList());
        return carList;
    }

    /**
     * @Author dinghui
     * @Description 计算近期连续出现冷号的期数（将前五个车辆号定为热号，后五个车辆号定为冷号）
     * @Date 21:52 2024/1/7
     * @Param [schema2DTO]
     * @return com.dax.racing.dto.Schema2AppearColdCountDTO
     **/
    private Schema2AppearColdCountDTO calculateSchema2AppearColdCountResult(Schema2DTO schema2DTO) {
        Schema2AppearColdCountDTO schema2AppearColdCountDTO = null;
        if (schema2DTO != null && !CollectionUtils.isEmpty(schema2DTO.getCar1CarSort())) {
            schema2AppearColdCountDTO = new Schema2AppearColdCountDTO();

            //取出十辆车对应的五个冷号
            List<CarDTO> car1coldNumList = schema2DTO.getCar1CarSort().subList(Math.max(0, schema2DTO.getCar1CarSort().size() - 5), schema2DTO.getCar1CarSort().size());
            List<CarDTO> car2coldNumList = schema2DTO.getCar2CarSort().subList(Math.max(0, schema2DTO.getCar2CarSort().size() - 5), schema2DTO.getCar2CarSort().size());
            List<CarDTO> car3coldNumList = schema2DTO.getCar3CarSort().subList(Math.max(0, schema2DTO.getCar3CarSort().size() - 5), schema2DTO.getCar3CarSort().size());
            List<CarDTO> car4coldNumList = schema2DTO.getCar4CarSort().subList(Math.max(0, schema2DTO.getCar4CarSort().size() - 5), schema2DTO.getCar4CarSort().size());
            List<CarDTO> car5coldNumList = schema2DTO.getCar5CarSort().subList(Math.max(0, schema2DTO.getCar5CarSort().size() - 5), schema2DTO.getCar5CarSort().size());
            List<CarDTO> car6coldNumList = schema2DTO.getCar6CarSort().subList(Math.max(0, schema2DTO.getCar6CarSort().size() - 5), schema2DTO.getCar6CarSort().size());
            List<CarDTO> car7coldNumList = schema2DTO.getCar7CarSort().subList(Math.max(0, schema2DTO.getCar7CarSort().size() - 5), schema2DTO.getCar7CarSort().size());
            List<CarDTO> car8coldNumList = schema2DTO.getCar8CarSort().subList(Math.max(0, schema2DTO.getCar8CarSort().size() - 5), schema2DTO.getCar8CarSort().size());
            List<CarDTO> car9coldNumList = schema2DTO.getCar9CarSort().subList(Math.max(0, schema2DTO.getCar9CarSort().size() - 5), schema2DTO.getCar9CarSort().size());
            List<CarDTO> car10coldNumList = schema2DTO.getCar10CarSort().subList(Math.max(0, schema2DTO.getCar10CarSort().size() - 5), schema2DTO.getCar10CarSort().size());

            //各车辆冷号累计次数
            schema2AppearColdCountDTO.setCar1AppearColdCount(countSchema2AppearColdNum(1, car1coldNumList));
            schema2AppearColdCountDTO.setCar2AppearColdCount(countSchema2AppearColdNum(2, car2coldNumList));
            schema2AppearColdCountDTO.setCar3AppearColdCount(countSchema2AppearColdNum(3, car3coldNumList));
            schema2AppearColdCountDTO.setCar4AppearColdCount(countSchema2AppearColdNum(4, car4coldNumList));
            schema2AppearColdCountDTO.setCar5AppearColdCount(countSchema2AppearColdNum(5, car5coldNumList));
            schema2AppearColdCountDTO.setCar6AppearColdCount(countSchema2AppearColdNum(6, car6coldNumList));
            schema2AppearColdCountDTO.setCar7AppearColdCount(countSchema2AppearColdNum(7, car7coldNumList));
            schema2AppearColdCountDTO.setCar8AppearColdCount(countSchema2AppearColdNum(8, car8coldNumList));
            schema2AppearColdCountDTO.setCar9AppearColdCount(countSchema2AppearColdNum(9, car9coldNumList));
            schema2AppearColdCountDTO.setCar10AppearColdCount(countSchema2AppearColdNum(10, car10coldNumList));
        }

        return schema2AppearColdCountDTO;
    }

    /**
     * @Author dinghui
     * @Description 统计车道连续出现冷号的次数
     * @Date 22:39 2024/1/7
     * @Param [carNum, coldCarDTOList]
     * @return int
     **/
    private int countSchema2AppearColdNum(int carNum, List<CarDTO> coldCarDTOList) {
        List<Integer> coldList = coldCarDTOList.stream().map(CarDTO::getCarNum).collect(Collectors.toList());

        //当前的车道号码
        int currentLaneNum = 0;
        //冷号累计次数
        int coldCount = 0;

        //如果最新一期的开奖记录是冷号，则累加
        //否则直接返回 0
        if (coldList.contains(carNum)) {
            coldCount++;
        } else {
            return 0;
        }

        //根据最后一次开奖记录，初始化相关信息
        RacingResultDTO racingResultDTO = allList.get(0);
        if (carNum == racingResultDTO.getLane1CarNum()) {
            currentLaneNum = 1;
        } else if (carNum == racingResultDTO.getLane2CarNum()) {
            currentLaneNum = 2;
        } else if (carNum == racingResultDTO.getLane3CarNum()) {
            currentLaneNum = 3;
        } else if (carNum == racingResultDTO.getLane4CarNum()) {
            currentLaneNum = 4;
        } else if (carNum == racingResultDTO.getLane5CarNum()) {
            currentLaneNum = 5;
        } else if (carNum == racingResultDTO.getLane6CarNum()) {
            currentLaneNum = 6;
        } else if (carNum == racingResultDTO.getLane7CarNum()) {
            currentLaneNum = 7;
        } else if (carNum == racingResultDTO.getLane8CarNum()) {
            currentLaneNum = 8;
        } else if (carNum == racingResultDTO.getLane9CarNum()) {
            currentLaneNum = 9;
        } else if (carNum == racingResultDTO.getLane10CarNum()) {
            currentLaneNum = 10;
        }

        //逐条判断历史开奖记录
        for (int i = 1; i < allList.size(); i++) {
            if (allList.get(i) != null) {
                RacingResultDTO dto = allList.get(i);
                //1. 本次开奖记录，相同车道的车号是否为冷号
                //若为冷号，则计数 +1
                //否则，跳出循环
                if (currentLaneNum == 1) {
                    if (coldList.contains(dto.getLane1CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 2) {
                    if (coldList.contains(dto.getLane2CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 3) {
                    if (coldList.contains(dto.getLane3CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 4) {
                    if (coldList.contains(dto.getLane4CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 5) {
                    if (coldList.contains(dto.getLane5CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 6) {
                    if (coldList.contains(dto.getLane6CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 7) {
                    if (coldList.contains(dto.getLane7CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 8) {
                    if (coldList.contains(dto.getLane8CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 9) {
                    if (coldList.contains(dto.getLane9CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                } else if (currentLaneNum == 10) {
                    if (coldList.contains(dto.getLane10CarNum())) {
                        coldCount++;
                    } else {
                        break;
                    }
                }

                //2. 若上一步判断为冷号，则将本次相同车号的车道信息保存，以供上一期历史开奖记录判断之用
                if (carNum == dto.getLane1CarNum()) {
                    currentLaneNum = 1;
                } else if (carNum == dto.getLane2CarNum()) {
                    currentLaneNum = 2;
                } else if (carNum == dto.getLane3CarNum()) {
                    currentLaneNum = 3;
                } else if (carNum == dto.getLane4CarNum()) {
                    currentLaneNum = 4;
                } else if (carNum == dto.getLane5CarNum()) {
                    currentLaneNum = 5;
                } else if (carNum == dto.getLane6CarNum()) {
                    currentLaneNum = 6;
                } else if (carNum == dto.getLane7CarNum()) {
                    currentLaneNum = 7;
                } else if (carNum == dto.getLane8CarNum()) {
                    currentLaneNum = 8;
                } else if (carNum == dto.getLane9CarNum()) {
                    currentLaneNum = 9;
                } else if (carNum == dto.getLane10CarNum()) {
                    currentLaneNum = 10;
                }
            }
        }

        return coldCount;
    }

    /**
     * @Author dinghui
     * @Description 计算方案三的结果
     * 【方案三】
     *  每条车道开出的每一期结果大小单双的错误期数或正确期数（最好是错误的期数）
     *
     * @Date 0:31 2023/12/7
     * @Param []
     * @return com.dax.racing.dto.Schema3DTO
     **/
    private Schema3DTO calculateSchema3Result() {
        /*
        	车道 1：出现车号 8，计算效果如下
                    大	小	单	双
            正确次数	1	0	0	1
            错误次数	0	1	1	0
         */
        Schema3DTO schema3DTO = null;
        if (!CollectionUtils.isEmpty(allList)) {
            //车道 1
            Schema3LaneDTO lane1 = createSchema3LaneDTO(null);
            //车道 2,
            Schema3LaneDTO lane2 = createSchema3LaneDTO(null);
            //车道 3,
            Schema3LaneDTO lane3 = createSchema3LaneDTO(null);
            //车道 4,
            Schema3LaneDTO lane4 = createSchema3LaneDTO(null);
            //车道 5,
            Schema3LaneDTO lane5 = createSchema3LaneDTO(null);
            //车道 6,
            Schema3LaneDTO lane6 = createSchema3LaneDTO(null);
            //车道 7,
            Schema3LaneDTO lane7 = createSchema3LaneDTO(null);
            //车道 8,
            Schema3LaneDTO lane8 = createSchema3LaneDTO(null);
            //车道 9,
            Schema3LaneDTO lane9 = createSchema3LaneDTO(null);
            //车道 10
            Schema3LaneDTO lane10 = createSchema3LaneDTO(null);
            for (RacingResultDTO racingResultDTO : allList) {
                if (racingResultDTO != null) {
                    //逐条车道累计次数
                    sumSchema3ResultCount(lane1, racingResultDTO.getLane1CarNum());
                    sumSchema3ResultCount(lane2, racingResultDTO.getLane2CarNum());
                    sumSchema3ResultCount(lane3, racingResultDTO.getLane3CarNum());
                    sumSchema3ResultCount(lane4, racingResultDTO.getLane4CarNum());
                    sumSchema3ResultCount(lane5, racingResultDTO.getLane5CarNum());
                    sumSchema3ResultCount(lane6, racingResultDTO.getLane6CarNum());
                    sumSchema3ResultCount(lane7, racingResultDTO.getLane7CarNum());
                    sumSchema3ResultCount(lane8, racingResultDTO.getLane8CarNum());
                    sumSchema3ResultCount(lane9, racingResultDTO.getLane9CarNum());
                    sumSchema3ResultCount(lane10, racingResultDTO.getLane10CarNum());
                }
            }

            //填入各车道结果
            schema3DTO = new Schema3DTO();
            schema3DTO.setLane1CountDTO(lane1);
            schema3DTO.setLane2CountDTO(lane2);
            schema3DTO.setLane3CountDTO(lane3);
            schema3DTO.setLane4CountDTO(lane4);
            schema3DTO.setLane5CountDTO(lane5);
            schema3DTO.setLane6CountDTO(lane6);
            schema3DTO.setLane7CountDTO(lane7);
            schema3DTO.setLane8CountDTO(lane8);
            schema3DTO.setLane9CountDTO(lane9);
            schema3DTO.setLane10CountDTO(lane10);
        }
        return schema3DTO;
    }

    /**
     * 创建方案三的车道对象
     * @param schema3LaneDTO
     * @return
     */
    private Schema3LaneDTO createSchema3LaneDTO(Schema3LaneDTO schema3LaneDTO) {
        if (schema3LaneDTO == null) {
            schema3LaneDTO = new Schema3LaneDTO();
        }

        schema3LaneDTO.setCorrectCountDTO(new Schema3ResultCountDTO(0, 0, 0, 0));
        schema3LaneDTO.setErrorCountDTO(new Schema3ResultCountDTO(0, 0, 0, 0));
        return schema3LaneDTO;
    }

    /**
     * 累计方案三的各车道结果次数
     * @param schema3LaneDTO
     * @param carNum
     * @return
     */
    private Schema3LaneDTO sumSchema3ResultCount(Schema3LaneDTO schema3LaneDTO, int carNum) {
        if (schema3LaneDTO != null && carNum >= 1 || carNum <= 10) {
            //计算单、双
            if (carNum % 10 == 0) {
                //双数（偶数）
                schema3LaneDTO.getCorrectCountDTO().setEvenCount(schema3LaneDTO.getCorrectCountDTO().getEvenCount() + 1);
                schema3LaneDTO.getErrorCountDTO().setOddCount(schema3LaneDTO.getErrorCountDTO().getOddCount() + 1);
            } else {
                //单数（奇数）
                schema3LaneDTO.getCorrectCountDTO().setOddCount(schema3LaneDTO.getCorrectCountDTO().getOddCount() + 1);
                schema3LaneDTO.getErrorCountDTO().setEvenCount(schema3LaneDTO.getErrorCountDTO().getEvenCount() + 1);
            }

            //计算大、小
            if (carNum >= 6) {
                //大数
                schema3LaneDTO.getCorrectCountDTO().setBigCount(schema3LaneDTO.getCorrectCountDTO().getBigCount() + 1);
                schema3LaneDTO.getErrorCountDTO().setSmallCount(schema3LaneDTO.getErrorCountDTO().getSmallCount() + 1);
            } else {
                //小数
                schema3LaneDTO.getCorrectCountDTO().setSmallCount(schema3LaneDTO.getCorrectCountDTO().getSmallCount() + 1);
                schema3LaneDTO.getErrorCountDTO().setBigCount(schema3LaneDTO.getErrorCountDTO().getBigCount() + 1);
            }
        }
        return schema3LaneDTO;
    }

    /**
     * @Author dinghui
     * @Description 一次性获取 N 天的数据
     * @Date 0:04 2023/12/7
     * @Param [webLoginCookie, dayNum]
     * @return void
     **/
    private void getRacingDataWithDayNum(String webLoginCookie, int dayNum) {
        try {
            String[] dateArr = DateUtils.getLastNDayDateStrArr(dayNum);
            if (dateArr != null && dateArr.length > 0) {
                for (String date : dateArr) {
                    if (StringUtils.isNotBlank(date)) {
//                        String url = Constants.REQUEST_URL_RACING_BY_DATE.replace(Constants.REQUEST_URL_RACING_REPLACE_DATE, date);
                        String url = siteAddress.substring(0, siteAddress.indexOf(".com") + 4) + Constants.REQUEST_URL_RACING_BY_DATE.replace(Constants.REQUEST_URL_RACING_REPLACE_DATE, date);
                        String dayHtml = HtmlUtils.sendRequestRacingByDate(url, getWebClient(), webLoginCookie);
                        logger.info("@@@    dayHtml = " + dayHtml);
                        List<RacingResultDTO> dayList = HtmlUtils.parsingRacingHtmlWithOneDay(dayHtml);
                        logger.info("日期：" + date);
                        logger.info("开奖期数：" + dayList.size());
                        if (!CollectionUtils.isEmpty(dayList)) {
                            allList.addAll(dayList);
                        }
                    }
                }
            }

            //将内存中的数据去重
            allList = allList.stream().distinct().collect(Collectors.toList());

            logger.info("开奖总期数和：" + allList.size());
            //关闭所有窗口
//            webClient.close();
        } catch (Exception e) {
            logger.error("处理异常，将 cookie 置空，不再重复错误请求。请联系管理员。", e);
            setWebCookie(null);
            e.printStackTrace();
        }
    }
}
