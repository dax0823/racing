package com.dax.racing.task;

import com.dax.racing.constants.Constants;
import com.dax.racing.service.RacingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @ClassName RacingTask
 * @Deacription 【赛车】定时任务
 * @Author dinghui
 * @Date 2023/12/4 0:13
 * @Version 1.0
 **/
@Component
public class RacingTask {
    private static Logger logger = LoggerFactory.getLogger(RacingTask.class);

    @Autowired
    private RacingService racingService;

    /**
     * @Author dinghui
     * @Description 每隔 N 秒执行一次。服务启动后运行
     * @Date 0:14 2023/12/4
     * @Param []
     * @return void
     **/
//    @Scheduled(fixedDelay = Constants.TIME_INTERVAL * 1000)
    @Scheduled(fixedRate = Constants.TIME_INTERVAL * 1000)
    public void getLastResult() {
        logger.warn("【赛车】获取最后一次开奖结果：开始……");

        racingService.getLastResult();

        logger.warn("【赛车】获取最后一次开奖结果：结束。");
    }

}
