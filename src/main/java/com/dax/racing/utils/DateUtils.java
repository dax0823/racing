package com.dax.racing.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @ClassName DateUtils
 * @Deacription 日期工具类
 * @Author dinghui
 * @Date 2023/12/3 23:22
 * @Version 1.0
 **/
public class DateUtils {
    private static final String DATE_FORMATTER_DEFAULT = "yyyy-MM-dd";

    /**
     * @Author dinghui
     * @Description 获取最后 N 天日期数组（格式：yyyy-MM-dd）
     * @Date 23:26 2023/12/3
     * @Param [dayNum]
     * @return java.lang.String[]
     **/
    public static String[] getLastNDayDateStrArr(int dayNum) {
        String[] dateArr = new String[dayNum];

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER_DEFAULT);
        LocalDate today = LocalDate.now();
        if (dayNum >= 1) {
            //仅有一天，即今天
            dateArr[0] = formatter.format(today);
        }

        if (dayNum > 1) {
            //不止一天，数组序号从第二个开始
            for (int i = 1; i < dayNum; i++) {
                dateArr[i] = formatter.format(today.minusDays(i));
            }
        }
        return dateArr;
    }

    /**
     * @Author dinghui
     * @Description 将毫秒值转成字符串（格式：yyyy-MM-dd）
     * @Date 23:24 2023/12/4
     * @Param [milliSeconds]
     * @return java.lang.String
     **/
    public static String getDateFromLong(long milliSeconds) {
        Instant instant = Instant.ofEpochMilli(milliSeconds);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER_DEFAULT).withZone(ZoneId.systemDefault());
        return formatter.format(instant);
    }
}
