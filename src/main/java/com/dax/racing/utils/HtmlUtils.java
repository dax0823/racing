package com.dax.racing.utils;

import com.alibaba.fastjson.JSONObject;
import com.dax.racing.dto.RacingResultDTO;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName HtmlUtil
 * @Deacription HtmlUtil工具类
 * @Author dinghui
 * @Date 2021/7/14 17:29
 * @Version 1.0
 **/
public class HtmlUtils {
    private static Logger logger = LoggerFactory.getLogger(HtmlUtils.class);
    /**
     * @Author dinghui
     * @Description 创建 WebClient 对象
     * @Date 15:33 2021/7/22
     * @Param []
     * @return com.gargoylesoftware.htmlunit.WebClient
     **/
    public static WebClient createWebClient() throws Exception {
        return HtmlUtils.createWebClient(null, null);
    }

    /**
     * @Author dinghui
     * @Description 创建 WebClient 对象
     * @Date 15:33 2021/7/22
     * @Param [ip, port]
     * @return com.gargoylesoftware.htmlunit.WebClient
     **/
    public static WebClient createWebClient(String ip, String port) throws Exception {
        WebClient client = null;
        if (StringUtils.isBlank(ip) || StringUtils.isBlank(port)) {
            client = new WebClient(BrowserVersion.CHROME);
        } else {
            client = new WebClient(BrowserVersion.CHROME, ip, Integer.valueOf(port));
        }
        //支持JavaScript
        client.getOptions().setUseInsecureSSL(true);
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        client.getOptions().setActiveXNative(false);
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setThrowExceptionOnFailingStatusCode(false);
        //设置连接超时时间
        client.getOptions().setTimeout(10000);
        //设置运行JavaScript的时间
        client.waitForBackgroundJavaScript(5000);
        return client;
    }

    /**
     * @Author dinghui
     * @Description 发送（一般）请求
     * @Date 15:33 2021/7/22
     * @Param [url, webClient, methodType, requestParameters, csrfToken]
     * @return java.lang.String
     **/
    public static String sendRequest(String url, WebClient webClient, HttpMethod methodType, List<NameValuePair> requestParameters,
                                     String csrfToken) throws Exception {
        if (StringUtils.isBlank(url)) {
            return null;
        }
        if (methodType == null) {
            methodType = HttpMethod.GET;
        }

        WebRequest request = new WebRequest(new URL(url), methodType);
        Map<String, String> additionalHeaders = new HashMap<>();
        additionalHeaders.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Maxthon/5.3.8.2000");
        additionalHeaders.put("Accept-Language", "zh-CN,zh;q=0.8");
        additionalHeaders.put("Accept", "*/*");
        request.setAdditionalHeaders(additionalHeaders);
        if (!CollectionUtils.isEmpty(requestParameters)) {
            request.setRequestParameters(requestParameters);
        }

        // 获取某网站页面 or 请求结果
        Page page = webClient.getPage(request);
        return page.getWebResponse().getContentAsString(Charset.forName("UTF-8"));
    }

    /**
     * @Author dinghui
     * @Description 按天获取开奖数据，发送请求（含 cookie）
     * @Date 1:22 2023/12/3
     * @Param [url, webClient, cookie]
     * @return java.lang.String
     **/
    public static String sendRequestRacingByDate(String url, WebClient webClient, String cookie) throws Exception {
        logger.info("@@@    sendRequestRacingByDate.url = " + url);
//        logger.info("@@@    sendRequestRacingByDate.webClient = " + JSONObject.toJSONString(webClient));
        logger.info("@@@    sendRequestRacingByDate.cookie = " + cookie);

        if (StringUtils.isBlank(url)) {
            return null;
        }

        WebRequest request = new WebRequest(new URL(url), HttpMethod.POST);
        Map<String, String> additionalHeaders = new HashMap<>();
        additionalHeaders.put("Cookie", cookie);
        request.setAdditionalHeaders(additionalHeaders);

        // 获取某网站页面 or 请求结果
        Page page = webClient.getPage(request);
        return page.getWebResponse().getContentAsString(Charset.forName("UTF-8"));
    }

    /**
     * @Author dinghui
     * @Description 解析【赛车】 html 结果
     * @Date 1:50 2023/12/3
     * @Param [html]
     * @return java.util.List<com.dax.racing.dto.RacingResultDTO>
     **/
    public static List<RacingResultDTO> parsingRacingHtmlWithOneDay(String html) {
        if (StringUtils.isBlank(html)) {
            return null;
        }

        List<RacingResultDTO> dtoList = new ArrayList<>(16);
        Document doc = Jsoup.parse(html);

        //取全部的开奖结果数据（<tbody><tr>***</tr></tbody>）
        Elements tbodyTags = doc.select("tbody");
        Elements trTags = tbodyTags.select("tr");
        for (Element tr : trTags) {
//            System.out.println(tr.text());
            if (tr.childrenSize() >= 20) {
                RacingResultDTO dto = new RacingResultDTO();
                //期数
                dto.setPeriod(tr.child(0).text() == null ? null : Long.parseLong(tr.child(0).text()));
                //开奖时间（格式：yyyy-MM-dd HH:mm:ss）
                dto.setDrawTime(tr.child(1).text() == null ? null : tr.child(1).text());

                //车道 1 的车辆编号
                Element span1 = tr.child(2).selectFirst("span");
                dto.setLane1CarNum(span1.text() == null ? 0 : Integer.parseInt(span1.text()));
                //车道 2 的车辆编号
                Element span2 = tr.child(3).selectFirst("span");
                dto.setLane2CarNum(span2.text() == null ? 0 : Integer.parseInt(span2.text()));
                //车道 3 的车辆编号
                Element span3 = tr.child(4).selectFirst("span");
                dto.setLane3CarNum(span3.text() == null ? 0 : Integer.parseInt(span3.text()));
                //车道 4 的车辆编号
                Element span4 = tr.child(5).selectFirst("span");
                dto.setLane4CarNum(span4.text() == null ? 0 : Integer.parseInt(span4.text()));
                //车道 5 的车辆编号
                Element span5 = tr.child(6).selectFirst("span");
                dto.setLane5CarNum(span5.text() == null ? 0 : Integer.parseInt(span5.text()));
                //车道 6 的车辆编号
                Element span6 = tr.child(7).selectFirst("span");
                dto.setLane6CarNum(span6.text() == null ? 0 : Integer.parseInt(span6.text()));
                //车道 7 的车辆编号
                Element span7 = tr.child(8).selectFirst("span");
                dto.setLane7CarNum(span7.text() == null ? 0 : Integer.parseInt(span7.text()));
                //车道 8 的车辆编号
                Element span8 = tr.child(9).selectFirst("span");
                dto.setLane8CarNum(span8.text() == null ? 0 : Integer.parseInt(span8.text()));
                //车道 9 的车辆编号
                Element span9 = tr.child(10).selectFirst("span");
                dto.setLane9CarNum(span9.text() == null ? 0 : Integer.parseInt(span9.text()));
                //车道 10 的车辆编号
                Element span10 = tr.child(11).selectFirst("span");
                dto.setLane10CarNum(span10.text() == null ? 0 : Integer.parseInt(span10.text()));

                //冠亚军之和
                dto.setGySum(tr.child(12).text() == null ? 0 : Integer.parseInt(tr.child(12).text()));
                //冠亚军之和的大小
                dto.setGyIsBig(tr.child(13).text() == null ? null : tr.child(13).text());
                //冠亚军之和的单双
                dto.setGyIsSingular(tr.child(14).text() == null ? null : tr.child(14).text());

                //车道 1 的龙虎
                dto.setLane1LH(tr.child(15).text() == null ? null : tr.child(15).text());
                //车道 2 的龙虎
                dto.setLane2LH(tr.child(16).text() == null ? null : tr.child(16).text());
                //车道 3 的龙虎
                dto.setLane3LH(tr.child(17).text() == null ? null : tr.child(17).text());
                //车道 4 的龙虎
                dto.setLane4LH(tr.child(18).text() == null ? null : tr.child(18).text());
                //车道 5 的龙虎
                dto.setLane5LH(tr.child(19).text() == null ? null : tr.child(19).text());

                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    /**
     * @Author dinghui
     * @Description 获取最后一次开奖结果
     * @Date 0:26 2023/12/4
     * @Param [url, webClient, cookie]
     * @return java.lang.String
     **/
    public static String sendRequestRacingLastResult(String url, WebClient webClient, String cookie) throws Exception {
        logger.info("@@@    sendRequestRacingLastResult.url = " + url);
//        logger.info("@@@    sendRequestRacingLastResult.webClient = " + JSONObject.toJSONString(webClient));
        logger.info("@@@    sendRequestRacingLastResult.cookie = " + cookie);

        if (StringUtils.isBlank(url)) {
            return null;
        }

        WebRequest request = new WebRequest(new URL(url), HttpMethod.POST);
        Map<String, String> additionalHeaders = new HashMap<>();
        additionalHeaders.put("Cookie", cookie);
        request.setAdditionalHeaders(additionalHeaders);

        // 获取某网站页面 or 请求结果
        Page page = webClient.getPage(request);
        return page.getWebResponse().getContentAsString(Charset.forName("UTF-8"));
    }

    /**
     * @Author dinghui
     * @Description 解析最后一次开奖数据
     * @Date 23:05 2023/12/4
     * @Param [lastResultJson]
     * @return com.dax.racing.dto.RacingResultDTO
     **/
    public static RacingResultDTO parsingLastResultJson(String lastResultJson) {
        if (StringUtils.isBlank(lastResultJson)) {
            return null;
        }

        /*
         * 解析 json 数据
         * 数据实例：{"lottery":"PK10JSC","drawNumber":"32985249","result":"10,6,9,5,8,7,1,3,4,2",
         * "detail":"LH1=L,2,冠军-龙;LH3=L,2,第三名-龙;LH5=L,3,第五名-龙;GDX=D,2,冠亚大;DX1=D,2,冠军-大;DX3=D,2,第三名-大;DX4=X,6,第四名-小;DX5=D,3,第五名-大;DX8=X,2,第八名-小;DX10=X,2,第十名-小;DS2=S,2,亚军-双;DS4=D,3,第四名-单;DS5=S,2,第五名-双;DS8=D,2,第八名-单;B5=8,2,第五名-8;",
         * "drawTime":1701702030000,"resultOther":"16,大,双,龙,龙,龙,龙,龙,"}
         **/
        RacingResultDTO dto = null;
        JSONObject jsonObject = JSONObject.parseObject(lastResultJson);
        if (jsonObject != null) {
            dto = new RacingResultDTO();
            //期数
            dto.setPeriod(jsonObject.getInteger("drawNumber"));

            //车道内的汽车编码
            String result = jsonObject.getString("result");
            if (StringUtils.isNotBlank(result)) {
                String[] carNumArr = result.split(",");
                if (carNumArr != null && carNumArr.length >= 10) {
                    dto.setLane1CarNum(carNumArr[0] == null ? 0 : Integer.parseInt(carNumArr[0]));
                    dto.setLane2CarNum(carNumArr[1] == null ? 0 : Integer.parseInt(carNumArr[1]));
                    dto.setLane3CarNum(carNumArr[2] == null ? 0 : Integer.parseInt(carNumArr[2]));
                    dto.setLane4CarNum(carNumArr[3] == null ? 0 : Integer.parseInt(carNumArr[3]));
                    dto.setLane5CarNum(carNumArr[4] == null ? 0 : Integer.parseInt(carNumArr[4]));
                    dto.setLane6CarNum(carNumArr[5] == null ? 0 : Integer.parseInt(carNumArr[5]));
                    dto.setLane7CarNum(carNumArr[6] == null ? 0 : Integer.parseInt(carNumArr[6]));
                    dto.setLane8CarNum(carNumArr[7] == null ? 0 : Integer.parseInt(carNumArr[7]));
                    dto.setLane9CarNum(carNumArr[8] == null ? 0 : Integer.parseInt(carNumArr[8]));
                    dto.setLane10CarNum(carNumArr[9] == null ? 0 : Integer.parseInt(carNumArr[9]));
                }
            }

            //开奖时间
            long milliSeconds = jsonObject.getLong("drawTime");
            dto.setDrawTime(DateUtils.getDateFromLong(milliSeconds));

            //其他值
            String resultOther = jsonObject.getString("resultOther");
            if (StringUtils.isNotBlank(resultOther)) {
                String[] otherArr = resultOther.split(",");
                if (otherArr != null && otherArr.length >= 8) {
                    //冠亚军
                    dto.setGySum(otherArr[0] == null ? 0 : Integer.parseInt(otherArr[0]));
                    dto.setGyIsBig(otherArr[1]);
                    dto.setGyIsSingular(otherArr[2]);

                    //龙虎榜
                    dto.setLane1LH(otherArr[3]);
                    dto.setLane2LH(otherArr[4]);
                    dto.setLane3LH(otherArr[5]);
                    dto.setLane4LH(otherArr[6]);
                    dto.setLane5LH(otherArr[7]);
                }
            }
        }
        return dto;
    }
}
