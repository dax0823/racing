package com.dax.racing.vo;

import com.dax.racing.dto.*;
import lombok.Data;

/**
 * @ClassName PageResultVO
 * @Deacription 页面请求结果
 * @Author dinghui
 * @Date 2023/12/6 23:37
 * @Version 1.0
 **/
@Data
public class ResponseVO {
    //初次加载时的天数
    private int loadDayNum;

    //当前累计的开奖期数
    private long periodCount;

    //最后一期开奖结果
    private RacingResultDTO lastRacingResultDTO;

    //方案一
    private Schema1DTO schema1DTO;

    //方案二
    private Schema2DTO schema2DTO;

    //方案二，冷/热车号出现次数累加和
    private Schema2AppearColdCountDTO schema2AppearColdCountDTO;

    //方案三
    private Schema3DTO schema3DTO;
}
