import ch.qos.logback.core.net.SyslogOutputStream;
import com.dax.racing.constants.Constants;

import javax.xml.bind.SchemaOutputResolver;

/**
 * @ClassName Test
 * @Deacription TODO
 * @Author dinghui
 * @Date 2024/2/25 22:19
 * @Version 1.0
 **/
public class Test {

    private String test() {

        String address = "https://www.f718.com";

        String str = address.substring(0, address.indexOf(".com") + 4) + Constants.REQUEST_URL_RACING_BY_DATE.replace(Constants.REQUEST_URL_RACING_REPLACE_DATE, "2024-02-25");

        System.out.println("substring = " + str);

        return null;
    }

    public static void main(String[] args) {
        System.out.println("测试开始……");

        Test test = new Test();
        test.test();

        System.out.println("测试结束。");
    }
}
